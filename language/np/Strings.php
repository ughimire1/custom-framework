<?php
return
    [


        'AboutCompany' => "कम्पनीकाे बारेमा",
        'GetInvolved' => 'जाेडिनुहाेस्',
        'DirectContact' => 'प्रत्यक्ष सम्पर्क',
        'EmailAddress' => "इमेल ठेगाना",
        "Password" => "पासवर्ड",
        "RememberMe" => "सुरक्षीत गर्नुहाेस्",
        "SignIn" => "साइन इन",
        "ForgotPasswordLink" => "पासवर्ड बिर्सीनुभयाे ?"


    ]


?>