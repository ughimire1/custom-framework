<?php

function is_ssl()
{
    if (isset($_SERVER['HTTPS'])) {
        if ('on' == strtolower($_SERVER['HTTPS']))
            return true;
        if ('1' == $_SERVER['HTTPS'])
            return true;
    } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
        return true;
    }
    return false;
}

function pp($array, $isDie = true)
{

    echo '<pre>';

    print_r($array);

    echo '</pre>';
    if ($isDie) {
        exit;
    }
}

function dd($array, $isDie = true)
{
    var_dump($array);
    if ($isDie) {
        exit;
    }
}

function Redirect($uri = null)
{

    header("Location:" . BASE_URL . $uri);

}

function GetResources($path = null)
{
    global $config;

    $layer = isset($config->Layer) ? $config->Layer : "web";

    if ($path == null) {

        $resources = "resources" . DS . $layer . DS;
    } else {

        $resources = "resources" . DS . $layer . DS . $path;
    }

    return BASE_URL . str_replace(DS, "/", $resources);
}

function CurrentTimestamp()
{
    return date('Y-m-d H:i:s');
}

function Resources($path = null)
{
    echo GetResources($path);
}

function GetSystemSalt()
{
    $numberOfFraction = 4; // Please do not change more than 12

    $numberOfMonthInAYear = 12;

    $currentDateTime = CurrentTimestamp();

    $monthNumber = date('m', strtotime($currentDateTime));

    $totalMonthRange = ($numberOfMonthInAYear / $numberOfFraction);

    $rangeNumber = ceil((int)$monthNumber / $totalMonthRange);

    $startMonth = ($rangeNumber - 1) * $totalMonthRange + 1;

    $endMonth = $startMonth + ($totalMonthRange - 1);

    $time = strtotime($currentDateTime);

    $month = date("F", $time);

    $year = date("Y", $time);

    $lastDayOfTheMonth = date('t', strtotime("{$year}-{$endMonth}-10"));

    $startFullDate = date('Y-m-d H:i:s', strtotime("{$year}-{$startMonth}-01 00:00:00"));

    $endFullDate = date('Y-m-d H:i:s', strtotime("{$year}-{$endMonth}-{$lastDayOfTheMonth} 23:59:59"));

    $saltString = "{Salt Start:  {$startFullDate} And Salt End: {$endFullDate}}";

    return array('salt' => hash("sha256", $saltString), 'range' => $rangeNumber);


}

function get(&$content)
{

    return isset($content) ? $content : "ERROR!! Undefined Variable Detected.";


}

function RandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}