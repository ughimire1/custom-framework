<?php

class autoloader
{

    public static function load($uri)
    {

        $lowerClass = strtolower(str_replace('\\', DS, BASE_PATH . DS . $uri));

        $class = (str_replace('\\', DS, BASE_PATH . DS . $uri));

        $layersLowerClass = strtolower(str_replace('\\', DS, LAYERS_PATH . DS . $uri));

        $layersClass = (str_replace('\\', DS, LAYERS_PATH . DS . $uri));

        if (self::isClassExists($lowerClass))//BASE_PATH.DS.
        {
            require_once($lowerClass . '.php');

        } else if (self::isClassExists($class))//BASE_PATH.DS.
        {
            require_once($class . '.php');

        } else if (self::isClassExists($layersLowerClass)) {

            require_once($layersLowerClass . '.php');

        } else if (self::isClassExists($layersClass)) {

            require_once($layersClass . '.php');


        } else {

            //throw new \Exception("Unable to load the requested class file: $lowerClass.php");
        }


    }

    private static function isClassExists($class)
    {

        if (file_exists($class . '.php'))//BASE_PATH.DS.
        {
            return true;

        } else {

            return false;
        }
    }

    public static function loadGlobal()
    {
        if (!empty(self::$directories)) {
            foreach (self::$directories as $directory) {
                $handle = opendir(path('app') . '\\' . PANEL . '\\' . $directory);
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        require_once path('app') . '\\' . PANEL . '\\' . $directory . '\\' . $entry;
                    }
                }
            }
            self::$directories = array();
        }
    }
}