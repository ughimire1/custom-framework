<?php

namespace System\Core;

class Loader
{
    public $site_js;

    public $config;

    public static $Shared = array();

    function __construct()
    {
        global $config;

        $this->config = $config;
    }

    public static function SharedView($View, $data = array())
    {
        global $config, $global, $language;
        if ($view = null) {

            die('Shared view empty');
        }

        $ViewArray = explode("::", $View);

        if (count($ViewArray) > 2) {

            die('Wrong parameter');
        }
        $module = isset($ViewArray[1]) ? $ViewArray[0] : $config->Module;

        $ViewName = isset($ViewArray[1]) ? $ViewArray[1] : $ViewArray[0];

        $filePath = LAYERS_PATH . DS . $config->Layer . DS . $module . DS . 'Views' . DS . $ViewName . '.php';

        if (file_exists($filePath)) {

            $mainData = (array(
                "Config" => $config,
                "Global" => $global,
                "Language" => $language
            ));
            extract(self::$Shared);
            extract($mainData);
            if (!empty($data)) {
                extract($data);
            }


            include_once($filePath);


        } elseif (file_exists(strtolower($filePath))) {

            include_once($filePath);

        } else {

            die('This file not loaded : ' . $filePath);
            throw new \Exception("Unable to load the requested view file: $viewFile.php");
        }


    }

    function Library($class, $directory = NULL)
    {

        $file = BASE_PATH . DS . 'Libraries' . DS . $directory . DS . $class . '.php';

        if (file_exists($file)) {

            if (class_exists($file) === FALSE) {

                require_once($file);

                return;
            } else
                throw new \Exception("Unable to load the requested class: $class");
        } else
            throw new \Exception("Unable to load the requested class file: $class.php");

    }

    public function View($viewFile, $data = array())
    {
        global $config, $global, $language;

        $filePath = LAYERS_PATH . DS . $this->config->Layer . DS . $this->config->Module . DS . 'Views' . DS . $viewFile . '.php';

        $mainData = (array(
            "Config" => $config,
            "Global" => $global,
            "Language" => $language
        ));

        extract(self::$Shared);
        
        extract($mainData);

        if (file_exists($filePath)) {

            if (!empty($data)) {
                extract($data);
            }


            include_once($filePath);


        } elseif (file_exists(strtolower($filePath))) {

            include_once($filePath);

        } else {

            die('This file not loaded : ' . $filePath);
            throw new \Exception("Unable to load the requested view file: $viewFile.php");
        }

    }

    function LoadJS($jsArray)
    {
        $this->site_js = "";
        if (count($jsArray) > 0) {
            foreach ($jsArray as $jsPath) {
                if (file_exists($jsPath)) {
                    $this->site_js .= "<script src='" . BASE_URL . "/" . $jsPath . "'></script>\n";
                }
            }
        }
        return $this->site_js;
    }


}