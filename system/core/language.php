<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/10/2016
 * Time: 10:42 PM
 */

namespace System\Core;


class Language
{


    function translate($strings, $parms = array())
    {


        $explodedString = explode('.', $strings);

        $filename = $explodedString[0];

        $index = $explodedString[1];

        $languageFile = LANGUAGE_PATH . DS . LANGUAGE . DS . $filename . '.php';


        if (!file_exists($languageFile)) {


            return $strings;
        }


        $languageStrings = require($languageFile);

        $languageText = $strings;


        if (isset($languageStrings[$index])) {

            $languageText = $languageStrings[$index];

        }

        foreach ($parms as $key => $val) {


            $languageText = str_replace(":" . $key, $val, $languageText);
        }

        return $languageText;
    }

}