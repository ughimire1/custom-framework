<?php

date_default_timezone_set("Asia/Kathmandu");

define('DS', DIRECTORY_SEPARATOR);

define('SYSTEM_PATH', BASE_PATH . DS . 'system');

define('SYSTEM_LIBRARIES_PATH', BASE_PATH . DS . 'system' . DS . 'libraries');

define('LAYERS_PATH', BASE_PATH . DS . 'apps' . DS . 'layers');

define('APP_LIBRARIES_PATH', BASE_PATH . DS . 'apps' . DS . 'libraries');

define('LANGUAGE_PATH', BASE_PATH . DS . 'language');

define('INI_PATH', BASE_PATH . DS . 'config' . DS . 'config.ini');

require_once(SYSTEM_LIBRARIES_PATH . DS . 'common.php');

$salt = GetSystemSalt();

define('SYSTEM_SALT', $salt['salt']);

define('SALT_RANGE', $salt['range']);

define('SESSION_NAME', hash('sha256', 'SESSION_' . $salt['salt'] . '_KEY'));

define('SESSION_LIFE', 10800);

session_name(SESSION_NAME);

session_save_path("tmp" . DS . 'session' . DS);

ini_set('session.gc_maxlifetime', SESSION_LIFE);

ini_set('session.gc_probability', 1);

ini_set('session.gc_divisor', 1);

session_set_cookie_params(SESSION_LIFE);

session_start();

header('Content-Type: text/html; charset=utf-8');

$serverType = "http://";

if (is_ssl()) {

    $serverType = "https://";

}
define('BASE_URL', $serverType . $_SERVER['HTTP_HOST'] . str_replace("index.php", "", $_SERVER['SCRIPT_NAME']));

$uri = $_SERVER['REQUEST_URI'];

$directory = str_replace('/index.php', "", substr($uri, 0, strpos($_SERVER['PHP_SELF'], '/index.php')));

$uri = str_replace($directory, "", $uri);

$uri = str_replace('index.php', "", $uri);

$uri = strpos($uri, '?') ? substr($uri, 0, strpos($uri, '?')) : $uri;

define('REQUESTED_URI', $uri);

define("URI", trim($uri, "/"));

define('COOKIE_SALT', hash("sha256", "COOKIE" . SYSTEM_SALT));

define('SESSION_SALT', hash("sha256", "SESSION" . SYSTEM_SALT));

require_once('autoloader.php');

spl_autoload_register(array('autoloader', 'load'));

define('COOKIE_LANG_KEY', 'LOADED_LANGUAGE');

$loadedLanguage = \Apps\Infrastructure\Cookie::check(COOKIE_LANG_KEY) ? \Apps\Infrastructure\Cookie::get(COOKIE_LANG_KEY) : "en";

define("LANGUAGE", $loadedLanguage);

global $config;

global $global;

global $language;

$language = new \System\Core\Language();

$global = new \stdClass();

$launcher = new \System\Core\Launcher($uri);

$launcher->loadGlobal();

$launcher->renderURI();

$launcher->launch();

?>