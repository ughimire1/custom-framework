<?php
namespace System\Core;

class Launcher
{

    public $modules;

    public $controller;

    public $method;

    public $args = array();

    protected $uri;

    protected $layer;

    protected $global;


    public function __construct($uri)
    {

        $this->uri = $uri;
    }


    public function launch()
    {

        $controllerPath = LAYERS_PATH . DS . $this->layer . DS . $this->modules . DS . 'Controllers' . DS . $this->controller . '.php';

        $controllerClass = ucwords($this->layer) . "\\" . ucwords($this->modules) . "\\Controllers\\" . $this->controller;


        if (file_exists($controllerPath) && class_exists($controllerClass) && method_exists($controllerClass, $this->method . 'Action')) {


            $controllerClassObj = new $controllerClass;

            $reflectionMethod = new \ReflectionMethod($controllerClass, $this->method . 'Action');

            $params = $reflectionMethod->getParameters();

            $paramArgs = array();

            foreach ($params as $param) {


                $className = ($param->getClass());

                if ($className != null) {

                    $classFullName = $className->name;

                } else {

                    $classFullName = "";
                }
                if (class_exists($classFullName)) {

                    array_push($paramArgs, new $className->name);

                } else {

                    array_push($paramArgs, null);
                }

            }
            $reflectionMethod->invokeArgs($controllerClassObj, $paramArgs);

            //call_user_func_array(array($controllerClassObj, $this->method . 'Action'), $this->args);

            //call_user_func_array(array($controllerClassObj, $this->method . 'Action'), $this->args);

            return;
        } else {

            die('<h1>DefaultController or Default moethods not found.');
        }


    }

    public function renderURI()
    {

        $uri = trim($this->uri, '/');

        $parts = explode('/', $uri);

        $registerPath = (LAYERS_PATH . DS . 'register.php');

        $registerLayers = require_once($registerPath);

        $registerLayerKeys = array_keys($registerLayers);

        $registerLayerValues = array_values($registerLayers);


        $layerIndex = null;


        if (in_array($parts[0], $registerLayerValues)) {

            $key = array_search($parts[0], $registerLayerValues);

            $layerIndex = $registerLayerKeys[$key];


            unset($parts[0]);

        } else {
            $layerIndex = "web";
        }


        $this->layer = $layerIndex;

        $layerRoutePath = LAYERS_PATH . DS . $this->layer . DS . 'routes.php';

        $layerRouteArray = require_once($layerRoutePath);


        if ((isset($parts[0]) && $parts[0] == "") || count($parts) == 0) {

            unset($parts);

            $routeIndex = "/";
        } else {

            if (isset($parts[0])) {

                array_push($parts, $parts[0]);

                unset($parts[0]);
            }
            $routeIndex = "{" . join('}/{', array_keys($parts)) . "}";

            $routeIndex = str_replace("{0}/", "", $routeIndex);
        }

        $newRoutesPartsIndex = null;
        if (isset($parts)) {

            $newRoutesPartsIndex = null;

            $numberOfMatchedTimes = 0;

            $numberOfEmptyParams = 0;

            foreach ($layerRouteArray as $keyRoute => $valueRoute) {


                $explodedRouteIndex = explode('/', $keyRoute);

                if (count($explodedRouteIndex) == count($parts)) {

                    $singleMatchedItem = 0;
                    $singleEmptyParam = 0;

                    foreach ($explodedRouteIndex as $singleRouteKey => $singleRouteValue) {

                        if ($singleRouteValue == "{" . ($singleRouteKey + 1) . "}") {

                            $singleEmptyParam++;
                        }


                        if ($singleRouteValue != "{" . ($singleRouteKey + 1) . "}" && (isset($parts[$singleRouteKey + 1]) && $parts[$singleRouteKey + 1] == $singleRouteValue)) {

                            $singleMatchedItem++;


                        }

                    }

                    if ($singleMatchedItem > $numberOfMatchedTimes) {

                        $numberOfMatchedTimes = $singleMatchedItem;

                        $newRoutesPartsIndex = $keyRoute;

                        $numberOfEmptyParams = $singleEmptyParam;

                    } else if ($singleMatchedItem == $numberOfMatchedTimes) {

                        if ($numberOfEmptyParams < $singleEmptyParam) {

                            $numberOfMatchedTimes = $singleMatchedItem;

                            $newRoutesPartsIndex = $keyRoute;

                            $numberOfEmptyParams = $singleEmptyParam;

                        } else {


                        }


                    }
                }


            }
        }


        $newRoutesParts = $newRoutesPartsIndex == null ? null : $layerRouteArray[$newRoutesPartsIndex];

        $this->modules = "shared";


        $this->controller = "DefaultController";

        $this->method = "Index";


        if ($newRoutesParts != null) {


            $routeConfig = $newRoutesParts;

            $routeArray = explode("/", $routeConfig);


            $this->modules = isset($routeArray[0]) ? $routeArray[0] : $this->modules;

            $this->controller = isset($routeArray[1]) ? substr($routeArray[1], 0, strpos($routeArray[1], "@")) : $this->controller;

            $this->method = isset($routeArray[1]) ? substr(strstr($routeArray[1], '@'), strlen('@')) : $this->method;


        } else if (isset($layerRouteArray[$routeIndex]) && $newRoutesParts == null) {


            $routeConfig = $layerRouteArray[$routeIndex];

            $routeArray = explode("/", $routeConfig);

            $this->modules = isset($routeArray[0]) ? $routeArray[0] : $this->modules;

            $this->controller = isset($routeArray[1]) ? substr($routeArray[1], 0, strpos($routeArray[1], "@")) : $this->controller;

            $this->method = isset($routeArray[1]) ? substr(strstr($routeArray[1], '@'), strlen('@')) : $this->method;
        }


        $controllerPath = LAYERS_PATH . DS . $this->layer . DS . $this->modules . DS . 'Controllers' . DS . $this->controller . ".php";

        $controllerClass = ucwords($this->layer) . "\\" . ucwords($this->modules) . "\\Controllers\\" . $this->controller;

        global $config;


        if (file_exists($controllerPath) && class_exists($controllerClass)) {

            if (!method_exists($controllerClass, $this->method . "Action")) {

                $this->modules = "shared";

                $this->controller = "DefaultController";

                $this->method = "MethodNotFound";
            }


        } else {

            $this->modules = "shared";

            $this->controller = "DefaultController";

            $this->method = "ControllerNotFound";
        }


        $config->Layer = $this->layer;

        $config->Module = $this->modules;

    }

    public function loadGlobal()
    {
        global $config;

        $config = new \stdClass();

        $config = ((object)parse_ini_file(INI_PATH, true));

        $this->global = $config;
    }
}