<?php
namespace System\MVC;

use System\Infrastructure\Request;

trait CRUDController
{

    abstract function SaveAction(Request $request);

    abstract function DeleteAction(Request $request);

    abstract function FindAction(Request $request);

    abstract function UpdateAction(Request $request);


}