<?php

namespace System\MVC;


use System\Core\Loader;


abstract class Controller
{
    private static $instance;

    public function Share($data = array())
    {
        if (!isset($this->Render)) {

            die("Shared not Found");

        }

        Loader::$Shared = array();
        Loader::$Shared = $data;
     
    }


    protected function  Controller()
    {

        self::$instance =& $this;

        $this->Render = new Loader();

    }

    public function translate($string, $param = array())
    {
        global $language;

        return $language->translate($string, $param);

    }


}