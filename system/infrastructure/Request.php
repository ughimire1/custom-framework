<?php
namespace System\Infrastructure;

class Request
{

    function get($index)
    {
        $data = $_GET;

        unset($_GET);

        if (isset($data[$index])) {

            return $data[$index];

        } else {

            return null;
        }

    }


    function post($index)
    {

        $data = $_POST;

        if (isset($data[$index])) {

            return $data[$index];

        } else {


            return null;
        }
    }

    function all()
    {
        $data = $_REQUEST;
        unset($_REQUEST);
        return $data;
    }

}