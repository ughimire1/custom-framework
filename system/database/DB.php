<?php

namespace System\Database;


class DB
{
    private static $dbconnection;

    public static function getConnection()
    {

        if (self::$dbconnection == null) {

            self::$dbconnection = self::dbConnect();

        }
        return self::$dbconnection;
    }

    private static function dbConnect()
    {
        try {


            global $config;


            $db = new \PDO("mysql:host=" . $config->Database['Host'] . ";port=" . $config->Database['Port'] . ";dbname=" . $config->Database['Database']
                , $config->Database['Username'], $config->Database['Password'], array(
                    \PDO::MYSQL_ATTR_LOCAL_INFILE => true));

            $db->exec("SET CHARACTER SET utf8");
            $db->exec("SET NAMES utf8");

            $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $db->exec("USE " . $config->Database['Database'] . ";");

            return $db;


        } catch (\Exception $e) {

            die($e->getMessage());
        }
    }


}