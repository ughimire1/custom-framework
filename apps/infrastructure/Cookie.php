<?php
namespace Apps\Infrastructure;
class Cookie
{


    static function get($cookie = null)
    {


        if ($cookie == null) {

            return isset($_COOKIE[COOKIE_SALT]) ? $_COOKIE[COOKIE_SALT] : array();
        }

        if (isset($_COOKIE[COOKIE_SALT][$cookie])) {

            return $_COOKIE[COOKIE_SALT][$cookie];

        } else {

            return null;
        }

    }

    static function set($cookie = array())
    {


        $path = BASE_PATH . DS . 'tmp' . DS . 'cookie' . DS;


        if (isset($cookie['name']) && isset($cookie['value'])) {

            $cookieName = COOKIE_SALT . "[$cookie[name]]";

            if (isset($cookie['duration'])) {

                setcookie($cookieName, $cookie['value'], $cookie['duration'], $path, $_SERVER['HTTP_HOST']);

            } else {
                setcookie($cookieName, $cookie['value'], 2000000000, $path, $_SERVER['HTTP_HOST']);

            }
            return true;

        } else {

            return false;

        }

    }

    static function check($cookie)
    {


        if (isset($_COOKIE[COOKIE_SALT][$cookie])) {

            return true;

        }
        return false;
    }

    static function remove($name)
    {


        setcookie(COOKIE_SALT . "[$name]", "", time() - 3600);

        return false;

    }

    static function  destroy()
    {

        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
    }

}