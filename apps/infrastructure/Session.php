<?php
namespace Apps\Infrastructure;

class Session
{
    static function get($session = null)
    {

        if ($session == null) {

            return isset($_SESSION[SESSION_SALT]) ? $_SESSION[SESSION_SALT] : array();
        }

        if (isset($_SESSION[SESSION_SALT][$session])) {

            return $_SESSION[SESSION_SALT][$session];

        } else {

            return null;
        }

    }

    static function set($key, $value)
    {

        $_SESSION[SESSION_SALT][$key] = $value;

        return true;


    }

    static function check($key)
    {
        if (isset($_SESSION[SESSION_SALT][$key])) {

            return true;

        }
        return false;
    }

    static function remove($name)
    {

        if (isset($_SESSION[SESSION_SALT])) {

            unset($_SESSION[SESSION_SALT]);
        }

        return true;
    }

    static function  destroy()
    {
        session_unset();

        session_destroy();

        return true;
    }

}