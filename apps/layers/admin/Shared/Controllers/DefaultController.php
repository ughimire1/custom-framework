<?php

namespace Admin\Shared\Controllers;

use System\MVC\Controller;

class DefaultController extends Controller
{


    function MethodNotFoundAction()
    {

        echo "Method Not found";
    }

    function ControllerNotFoundAction()
    {

        echo "Controller Not found";

    }
}