<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="<?php Resources('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/animate.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('includes/form-validation/css/validationEngine.jquery.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('includes/form-validation/css/template.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/styles.css'); ?>" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="card card-container">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>

        <p id="profile-name" class="profile-name-card"></p>

        <form class="form-signin" id="AdminLoginForm">
            <select name="language">
                <?php foreach (get($Languages) as $lang) { ?>
                    <option
                        value="<?= trim($lang); ?>"<?php echo (trim(strtoupper(LANGUAGE)) == trim(strtoupper($lang))) ? 'selected="selected"' : ''; ?>><?= strtoupper($lang); ?></option>
                <?php } ?>
            </select>
            <span id="reauth-email" class="reauth-email"></span>

            <input type="text" id="inputEmail" class="form-control validate[required,custom[email]]"
                   placeholder="<?= $language->translate("Strings.EmailAddress") ?>">


            <input type="password" id="inputPassword" class="form-control validate[required]"
                   placeholder="<?= $language->translate("Strings.Password") ?>">

            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> <?= $language->translate("Strings.RememberMe") ?>
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin"
                    type="submit"><?= $language->translate("Strings.SignIn") ?></button>
        </form>
        <!-- /form -->
        <a href="#" class="forgot-password">
            <?= $language->translate("Strings.ForgotPasswordLink") ?>
        </a>
    </div>
    <!-- /card-container -->
</div>
<!-- /container -->

<script type="text/javascript">

    var SESSION_SALT_ENCRYPTED = '<?php  echo substr(SESSION_SALT,-10).SESSION_SALT.substr(SESSION_SALT,0,10) ?>';

    var COOKIE_SALT_ENCRYPTED = '<?php echo substr(COOKIE_SALT, -10). COOKIE_SALT.substr(COOKIE_SALT, 0 ,10) ?>';

    var COOKIE_LANG_KEY = '<?php echo COOKIE_LANG_KEY; ?>';

</script>
<script type="text/javascript" src="<?php Resources('js/jquery-1.11.3.min.js'); ?>"></script>
<script type="text/javascript" src="<?php Resources('js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript"
        src="<?php Resources('includes/form-validation/js/languages/jquery.validationEngine-' . strtolower(LANGUAGE) . '.js'); ?>"></script>
<script type="text/javascript"
        src="<?php Resources('includes/form-validation/js/jquery.validationEngine.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/ajax-form-submit.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/Cookie.js'); ?>"></script>

<script type="text/javascript " src="<?php Resources('js/common.js'); ?>"></script>
</body>
</html>