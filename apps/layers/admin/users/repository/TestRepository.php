<?php

namespace Admin\AccessManagement\Repository;

use System\Database\DB;

use System\MVC\repo;

class TestRepository extends Repo
{

    function checkMethod()
    {

        $sqlQuery = DB::getConnection()->query("SELECT * from test");

        $data = $sqlQuery->fetch(\PDO::FETCH_ASSOC);
        return $data;
    }

}