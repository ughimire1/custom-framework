<?php
namespace Web\Home\Controllers;

use System\Infrastructure\Request;

use Web\WebController;

class HomeController extends WebController
{
    public function IndexAction(Request $request)
    {


        $this->Render->View("Home");
    }

}