<?php

self::SharedView("Shared::Header", array('page_title' => "Hello World"));

self::SharedView("Shared::TopBar", array());

?>
<div class="search-wrapper">

    <h2>500,00+ hotels, Resorts, Holiday Packages and more...</h2>

    <div class="point-search">

        <h2>Search a Hotels, Resorts, Holiday Packages, Destination on Nepal</h2>

        <input type="text" class="point-search-box"
               placeholder="Holiday Packages, Hotels, Resorts, Places and much more from Nepal"/>

        <input type="submit" class="search-submit" value="Search"/>
    </div>

</div>
<div class="container point-container search-grid">
    <div class="grid-title">
        <h3>Top Destinations</h3>
    </div>
    <div class="grids">


        <?php $destinations = array(1, 2, 3, 4, 5); ?>
        @foreach($destinations as $destination)

        <div class="col-lg-3 grid-item">
            <div class="gray-grid">


            </div>
            <div class="clearfix"></div>
            <div class="grid-detail">
                <h5>Destination</h5>

                <span class="grid-package">50 Packages</span>
                <span class="grid-agencies">40 Agencies</span>

                <div class="clearfix"></div>

            </div>

        </div>
        @endforeach

    </div>


</div>
<div class="clearfix"></div>
<div class="container point-container">

    <div class="point-newsletter">

        <h3>Become a member with Us</h3>

        <small>Improving the education capacity of under privileged and backward community</small>

        <a class="newsletter-join" href="">Join US Now </a>
    </div>
</div>
<?php
self::SharedView("Shared::Footer");
?>
