<?php
namespace Web;

use Apps\Infrastructure\Cookie;
use Apps\Infrastructure\Session;
use System\MVC\Controller;

class WebController extends Controller
{
    public function __construct()
    {
        parent::Controller();

        $languagePath = LANGUAGE_PATH . DS;

        $totalLanguages = scandir($languagePath);

        unset($totalLanguages[0]);

        unset($totalLanguages[1]);

        $shared['Languages'] = $totalLanguages;

        parent::Share($shared);


    }


}