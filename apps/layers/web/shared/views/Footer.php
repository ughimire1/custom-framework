<footer>
    <div class="container point-container">
        <div class="footer-detail col-lg-4">
            <h3><?= $language->translate('Strings.AboutCompany'); ?> </h3>
            <ul>

                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> About Pointnepal . com </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Privacy Policy </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Terms of Use</a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Travel Blog </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Media Room </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Culture of Nepal </a></li>
            </ul>


        </div>
        <div class="footer-detail col-lg-4">
            <h3><?= $language->translate('Strings.GetInvolved'); ?> </h3>


            <ul>

                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Boost your Business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Save time for sale your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> we are promote your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Save time for sale your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> we are promote your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Boost your Business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Save time for sale your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> we are promote your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> Save time for sale your business </a></li>
                <li><i class="fa fa-arrow-circle-o-right"></i><a href=""> we are promote your business </a></li>
            </ul>

        </div>
        <!--        <div class="footer-detail col-lg-3" > -->
        <!--            <h3>About Company </h3 > -->
        <!--            <ul>-->
        <!---->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > About Pointnepal . com </a ></li > -->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > Privacy Policy </a ></li > -->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > Terms of Use</a ></li > -->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > Travel Blog </a ></li > -->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > Media Room </a ></li > -->
        <!--                <li><i class="fa fa-arrow-circle-o-right" ></i ><a href = "" > Culture of Nepal </a ></li > -->
        <!--            </ul > -->
        <!---->
        <!--        </div > -->
        <div class="footer-detail col-lg-4 direct-contact">

            <h3><?= $language->translate('Strings.DirectContact'); ?> </h3>


            <span> Point Nepal Pvt . Ltd </span>
            <span> Jamal, way to Sumit Hotel </span>
            <span> Kathmandu Nepal,</span>
            <span> DAO Reg: ***, SWC Reg: *****,</span>
            <span> PAN Reg: ******</span>
            <span> Phone: +9779813296299 </span>
            <span> email: info@pointnepal . com </span>


        </div>
    </div>
    <div class="clearfix"></div>
    <div class="footer-bottom"></div>
</footer>

<script type="text/javascript">

    var SESSION_SALT_ENCRYPTED = '<?php  echo substr(SESSION_SALT,-10).SESSION_SALT.substr(SESSION_SALT,0,10) ?>';

    var COOKIE_SALT_ENCRYPTED = '<?php echo substr(COOKIE_SALT, -10). COOKIE_SALT.substr(COOKIE_SALT, 0 ,10) ?>';

    var COOKIE_LANG_KEY = '<?php echo COOKIE_LANG_KEY; ?>';


</script>
<script type="text/javascript" src="<?php Resources('js/jquery-1.11.3.min.js'); ?>"></script>
<script type="text/javascript" src="<?php Resources('js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript"
        src="<?php Resources('includes/form-validation/js/languages/jquery.validationEngine-' . strtolower(LANGUAGE) . '.js'); ?>"></script>
<script type="text/javascript"
        src="<?php Resources('includes/form-validation/js/jquery.validationEngine.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/ajax-form-submit.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/Cookie.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/home.js'); ?>"></script>
<script type="text/javascript " src="<?php Resources('js/common.js'); ?>"></script>
</body >
</html >