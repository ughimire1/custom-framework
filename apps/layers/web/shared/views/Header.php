<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title><?= get($page_title) ?></title>
    <link href="<?php Resources('css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/bootstrap-theme.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/animate.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('includes/form-validation/css/validationEngine.jquery.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('includes/form-validation/css/template.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php Resources('css/home.css'); ?>" rel="stylesheet">
</head>
<body class="main-page">