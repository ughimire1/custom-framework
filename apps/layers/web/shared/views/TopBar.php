<div class="top-bar">
    <div class="container point-container">

        <span class="login-register">

                      <ul class="nav navbar-nav navbar-right">


                          <li class="login_register">
                              <p><a
                                      onclick="LoadModel('@lang('Strings.LoginRegistrationForm')','{{ url('/agency/login/') }}','')"
                                      ><?= $language->translate("Message.Login"); ?></a> /
                                  <a href="{{ url('/agency/register') }}"> <?= $language->translate("Message.Register"); ?></a>

                              </p>

                              <div class="register_dialog"></div>
                          </li>

                          <?php if (isset($loginSet)) { ?>

                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                     aria-expanded="false">
                                      Username<span class="caret"></span>
                                  </a>

                                  <ul class="dropdown-menu dropdown-agency-menu" role="menu">
                                      <li><a href="{{ url('/profile') }}"><i
                                                  class="fa fa-btn fa-user"></i>Profile</a></li>
                                      <li><a href="{{ url('/logout') }}"><i
                                                  class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                  </ul>
                              </li>
                          <?php } ?>

                          <li><select name="language">
                                  <?php foreach (get($Languages) as $lang) { ?>
                                      <option
                                          value="<?= trim($lang); ?>"<?php echo (trim(strtoupper(LANGUAGE)) == trim(strtoupper($lang))) ? 'selected="selected"' : ''; ?>><?= strtoupper($lang); ?></option>
                                  <?php } ?>
                              </select></li>
                      </ul>
        </span>

    </div>

</div>