<?php
return [
    '/' => 'Home/HomeController@index',

    '{0}/{1}' => 'Method/MethodController@methodAction',
    '{1}/check/test/{4}' => 'Method/MethodController@method'
];