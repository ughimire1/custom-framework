(function ($) {
    $.fn.PointAjaxFormSubmit = function (options) {

        var defaultPrefix = "point-";

        var defaultOptions = {

            method: "POST",
            url: '/',
            message: '#message',
            callbefore: "",
            callback: "",
        };
        return this.each(function () {
            var $form = $(this);
            var self = {
                initialize: function () {

                    if ($form.attr(defaultPrefix + 'action') == undefined) {
                        throw  (defaultPrefix + "action attribute not defined in form tag");
                    }

                    if ($form.attr(defaultPrefix + 'message') == undefined) {
                        throw  (defaultPrefix + "message attribute not defined in form tag");
                    }

                    if ($form.attr(defaultPrefix + 'method') == undefined) {
                        throw  (defaultPrefix + "method attribute not defined in form tag");
                    }

                    if ($form.attr(defaultPrefix + 'callbefore') != undefined) {
                        defaultOptions.callbefore = $form.attr(defaultPrefix + 'callbefore');
                    }

                    if ($form.attr(defaultPrefix + 'callback') != undefined) {
                        defaultOptions.callback = $form.attr(defaultPrefix + 'callback');
                    }

                    defaultOptions.url = $form.attr(defaultPrefix + 'action');
                    defaultOptions.message = $form.attr(defaultPrefix + 'message');
                    defaultOptions.method = $form.attr(defaultPrefix + 'method');
                    var data = $form.serialize();
                    self.request(data);
                },
                request: function (data) {
                    $.ajax({
                        url: defaultOptions.url,
                        type: defaultOptions.method,
                        data: data,
                        beforeSend: function () {
                            if (typeof eval(defaultOptions.callbefore) == "function") {
                                window[defaultOptions.callbefore]($form, data);
                            }
                        },
                        complete: function (response) {
                            if (typeof eval(defaultOptions.callback) == "function") {
                                window[defaultOptions.callback]($form, response.responseText);
                            }

                            if ($(defaultOptions.message).length > 0) {

                                try {
                                    var jsonParse = $.parseJSON(response.responseText);
                                    var status = typeof jsonParse.Status != undefined ? jsonParse.Status : "";
                                    var messageParam = typeof jsonParse.Message != undefined ? jsonParse.Message : "";


                                    var messageText = "";

                                    if (typeof messageParam == "object") {


                                        $.each(messageParam, function (key, val) {

                                            messageText += "<li>" + val + "</li>";
                                        });
                                        if (messageText != "") {

                                            messageText = "<ul>" + messageText + "</ul>";
                                        }

                                    } else {
                                        messageText = messageParam;
                                    }


                                    if (status == "" || messageText == "") {
                                        throw ("Message or status not found");
                                    }
                                    $(defaultOptions.message).find('.alert').remove();
                                    var message = self.message(messageText, status);
                                    $(defaultOptions.message).append(message);

                                    if (status == "success") {

                                        $form.trigger('reset');

                                    }
                                } catch (ex) {

                                    var messageText = "Something error. Please try again.";
                                    $(defaultOptions.message).find('.alert').remove();
                                    var message = self.message(messageText, 'danger');
                                    $(defaultOptions.message).append(message);
                                }

                            }
                        }

                    });
                },
                message: function ($message, $status) {

                    var $messageBox = '<div class="alert alert-' + $status + '">';
                    $messageBox += '<i class="glyphicon glyphicon-' + $status + '-sign"></i>';
                    $messageBox += '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                    $messageBox += '<span style="float:left">' + $message + '</span><div style="clear:both"></div>';
                    $messageBox += '</div>';
                    return $messageBox;


                }

            };

            self.initialize();


        });

    };
})(jQuery);

/*$('body').on('submit', 'form.point-ajax', function (event) {


    event.preventDefault();
    $(this).PointAjaxFormSubmit({});

});*/

