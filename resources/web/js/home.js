$(function () {


    $('select[name="language"]').on('change', function () {


        Cookie.set(COOKIE_LANG_KEY, $(this).val());

        location.reload();
    });

    //LoadModel("Hello Header", "http://localhost/laravel/agency/register", "");
});


function LoadModel($header, $url, $data) {


    $header = typeof $header == "undefined" ? "Header" : $header;
    $url = typeof $url == "undefined" ? "" : $url;
    $data = typeof $url == "object" ? $data : "";
    var modal = $("<div class='modal fade' role='dialog'/>");
    modal.append('<div class="modal-dialog modal-lg"><div class="modal-content ' + class_prefix + 'modal-content"></div></div>');
    var modalHeader = '<div class="modal-header ' + class_prefix + 'modal-header">' +
        '<button type="button" class="close" data-dismiss="modal">' +
        '&times;' +
        '</button>' +
        '<h4 class="modal-title">' + $header + '</h4>' +
        '</div>';
    var modalBody = $('<div class="modal-body"/>');

    if ($url != "") {
        $.ajax({
            url: $url,
            type: "GET",
            data: $data,
            beforeSend: function () {

            },
            complete: function (response) {

                modalBody.append(response.responseText);
            }

        });
    }

    var modalFooter = $('<div class="modal-footer"/>');
    var closeButton = '';//'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
    modalFooter.append(closeButton);

    modal.find('.modal-content').append(modalHeader).append(modalBody).append(modalFooter);

    modal.modal();
}
