$(function () {


    jQuery("#AdminLoginForm").validationEngine('attach', {
        relative: true,
        promptPosition: "topRight"


    });

    $('select[name="language"]').on('change', function () {


        Cookie.set(COOKIE_LANG_KEY, $(this).val());

        location.reload();
    });


});


