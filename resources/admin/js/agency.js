/**
 * Created by hp on 11/08/2016.
 */
$(function () {

    $.fn.pointhover = function (options) {


        $this = $(this);


        $.each($this, function () {

            var $obj = $(this);

            var methods = {


                init: function () {


                    this.bind($obj, 'click', this.objClicked);
                },

                bind: function ($obj, event, method) {

                    $obj.bind(event, method);
                },
                objClicked: function (event) {



                    //alert('clicked');
                }


            }


            methods.init();


        });
    }


});

$(function () {


    $('body').on('click', '.point-add-button', function () {


        var $btnParent = $(this).parent('.point-button-wrapper');


        var clonedContent = $btnParent.clone();

        clonedContent.find('input').val("");

        //clonedContent.find('.point-add-button').addClass('point-minus-button').removeClass('point-add-button');

        $btnParent.after(clonedContent);


        $btnParent.find('.point-add-button').addClass('point-minus-button').removeClass('point-add-button');


    });


    $('body').on('click', '.point-minus-button', function () {

        var $btnParent = $(this).parent('.point-button-wrapper');

        $btnParent.remove();

    });

    $('body').on('click', '.add-new-person', function () {


        var ele = $(this).closest('form').find('.contact-person-form:last-of-type');

        var clonedContent = ele.clone();

        clonedContent.append($("<div class='minus-person' title='Delete this contact person' onclick='DeleteContactPerson(this)'/>"));

        clonedContent.find('input').val("");

        //clonedContent.find('.point-add-button').addClass('point-minus-button').removeClass('point-add-button');

        ele.after(clonedContent);

    })


// Link to open the dialog
    $("#dialog-link").click(function (event) {
        $("#dialog").dialog("open");
        event.preventDefault();
    });


    $('.bug-report').pointhover();

});
function DeleteContactPerson($node) {


    $node.closest('.contact-person-form').remove();
}

function Confirmation($url, $token) {

    var clickedNode = $(event.srcElement);

    var width = 300;

    var height = 150;

    var formTitle = "Confirmation";

    var $dialog = $('<div id="dialog" title="' + formTitle + '"/>');

    var ajaxData = {_token: $token}

    $dialog.dialog({
        autoOpen: false,
        width: width,
        height: height,
        buttons: {
            Yes: function () {


                $(this).dialog("close");

                var $form = $('<form method="post" action="' + $url + '"/>');
                $form.append('<input type="hidden" name="_token" value="' + $token + '" />');
                $form.append('<input type="hidden" name="ID" value="' + clickedNode.attr('data-id') + '" />');
                //clickedNode.after($form);
                $form.submit();
            },
            No: function () {
                //doFunctionForNo();
                $(this).dialog("close");
            }
        },
        close: function (event, ui) {
            $(this).remove();
        }
    });

    $dialog.dialog("open");
    $dialog.html("Are you sure want to delete ?");


}

function ShowDialog($url, $token, formTitle, width, height, callBack) {

    var clickedNode = $(event.srcElement);

    if (width == undefined) {

        width = 600;

    }

    if (height == undefined) {


        height = 350;
    }


    var $dialog = $('<div id="dialog" title="' + formTitle + '"/>');

    OpenDialog($url, $dialog, width, height);

    var ajaxData = {_token: $token}
    if (typeof callBack != "undefined") {


        var extraParam = callBack(clickedNode);

        ajaxData = {_token: $token, params: extraParam}
    }


    $.ajax({
        url: $url,
        type: 'POST',
        data: ajaxData,
        beforeSend: function (xhr) {

        }, complete: function (response) {

            $dialog.html(response.responseText);

        }
    });

}
function OpenDialog($url, $dialog, width, height) {


    // $dialog.html('<p>Umesh Ghimire Dialog</p>');
    $dialog.dialog({
        autoOpen: false,
        width: width,
        height: height
        /*buttons: [
         {
         text: "Ok",
         click: function () {
         $(this).dialog("close");
         }
         },
         {
         text: "Cancel",
         click: function () {
         $(this).dialog("close");
         }
         }
         ]*/
    });

    $dialog.dialog("open");
}






