var Cookie = function () {

    //this.salt = COOKIE_SALT_ENCRYPTED;


    this.get = function (cookieIndex) {


        if (cookieIndex == undefined) {
            throw ("Cookie index parameter not found");
        }


        var salt = this.getSalt();

        var cookie = document.cookie;

        var partsOfStr = cookie.split(';');

        var cookieValue = "";

        for (var i = 0; i < partsOfStr.length; i++) {

            var cookieParts = partsOfStr[i].split('=');

            if (cookieParts.length > 1) {

                var saltIndex = (cookieParts[0].indexOf(salt));

                if (saltIndex != -1) {

                    var cookieString = cookieParts[0].replace(salt, "").trim();

                    var partsOfCookie = cookieString.split(cookieIndex);

                    if (partsOfCookie.length > 1) {


                        cookieValue = cookieParts[1];

                        break;

                    }

                }

            }
        }


        return (cookieValue);
    };
    this.set = function (key, val) {


        if (key == undefined || val == undefined) {
            throw ("Cookie key and value not set as parameter");
        }
        document.cookie = this.getSalt() + "[" + key + "]=" + val;

        return true;

    }
    this.getSalt = function () {
        var replaceFirst = COOKIE_SALT_ENCRYPTED.substring(10);
        var final = replaceFirst.slice(0, -10);
        return final;
    };

};
var Cookie = new Cookie();
